# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0

CC ?= cc
CFLAGS ?= -O2

tcpserver: tcpserver.c
	$(CC) -o $@ -Wall -Wextra $(CFLAGS) $<

clean:
	rm -f tcpserver
