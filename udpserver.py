#!/usr/bin/python
"""
Copyright (C) Internet Systems Consortium, Inc. ("ISC")

SPDX-License-Identifier: MPL-2.0

Fake UDP server responding to SOA queries.
To be used together with tcpserver in case the transfer client under test does
an UDP query first.
"""

import argparse
import socket
import time

import dns.message
import dns.name


def gen_response(inmsg, serial):
    """
    Generate answer with one SOA RR in the ANSWER section, copying qname.
    If serial is None use current unix time.
    Other types result in NOTIMP.
    """
    outmsg = dns.message.make_response(inmsg)
    if len(inmsg.question) != 1:
        outmsg.set_rcode(dns.rcode.FORMERR)
        return outmsg
    question = inmsg.question[0]
    if question.rdtype != dns.rdatatype.SOA:
        outmsg.set_rcode(dns.rcode.NOTIMP)
        return outmsg
    if serial is None:
        serial = int(time.time())
    outmsg.flags |= dns.flags.AA
    outmsg.answer.append(
        dns.rrset.from_text_list(
            name=question.name,
            ttl=1,
            rdclass=question.rdclass,
            rdtype=question.rdtype,
            text_rdatas=[f". . {serial} 3600 1 1 1"],
        )
    )
    return outmsg


def main():
    parser = argparse.ArgumentParser(
        description="UDP server which responds only to with SOA queries"
    )

    parser.add_argument(
        "--serial",
        required=False,
        type=int,
        help="Serial to return; defaults to current Unix time",
    )
    parser.add_argument(
        "port", nargs="?", default=53, type=int, help="UDP port to listen on"
    )
    args = parser.parse_args()

    sock = socket.socket(type=socket.SOCK_DGRAM)
    sock.bind(("", args.port))

    while True:
        blob, _ancdata, _msg_flags, clientaddr = sock.recvmsg(65535)
        inmsg = dns.message.from_wire(blob)
        outmsg = gen_response(inmsg, args.serial)
        sock.sendmsg((outmsg.to_wire(),), [], 0, clientaddr)


if __name__ == "__main__":
    main()
