/*
 * Copyright (C) Internet Systems Consortium, Inc. ("ISC")
 *
 * SPDX-License-Identifier: MPL-2.0
 *
 * Minimalistic TCP server which delivers blobs from file on disk
 * as fast as it can, and modifies DNS message ID in the stream
 * to specified value used by client in the initial request.
 *
 * Input file must be valid TCP-stream binary format as specified
 * by RFC 1035 section 4.2.2. TCP usage. It does not do any checks
 * whatsoever.
 */

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/uio.h>
#include <unistd.h>

static int
get_socket(int port) {
	struct sockaddr_in addr;
#if defined(BUFSIZE)
	int bufsize = BUFSIZE;
#endif

	int fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (fd < 0) {
		perror("socket");
		return -1;
	}
	if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &(int){ 1 }, sizeof(int)) < 0) {
		perror("setsockopt(SO_REUSEADDR)");
	}

#if defined(BUFSIZE)
	if (setsockopt(fd, SOL_SOCKET, SO_RCVBUF, &bufsize, sizeof(bufsize)) < 0) {
		perror("setsockopt(SO_RCVBUF)");
	}

	if (setsockopt(fd, SOL_SOCKET, SO_SNDBUF, &bufsize, sizeof(bufsize)) < 0) {
		perror("setsockopt(SO_SENDBUF)");
	}
#endif

	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = INADDR_ANY;
	addr.sin_port = htons(port);

	if (bind(fd, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
		perror("bind");
		return -1;
	}

	if (listen(fd, 1) != 0) {
		perror("listen");
		return -1;
	}

	return fd;
}

int
get_msgid(int clientfd) {
	unsigned char recvbuf[sizeof(uint16_t) + 0xffff];
	uint16_t recvlen = 0;
	while (recvlen < 2) {
		int ret = read(clientfd, recvbuf + recvlen, 2);
		if (ret == -1) {
			if (errno == EINTR) {
				continue;
			} else {
				perror("recv msg len");
				return -1;
			}
		}
		recvlen += ret;
	}
	uint16_t msglen = ntohs(*((uint16_t *)recvbuf));
	if (msglen < 2) {
		fprintf(stderr, "weird message length: %d\n", msglen);
		return -2;
	}
	/*
	 * read whole message just to make sender happy,
	 * we care only about the message ID
	 */
	while (recvlen < msglen + 2) {
		int ret = read(clientfd, recvbuf + recvlen, msglen - (recvlen - 2));
		if (ret == -1) {
			if (errno == EINTR) {
				continue;
			} else {
				perror("recv msg");
				return -3;
			}
		}
		recvlen += ret;
	}
	uint16_t msgid = *((uint16_t *)(recvbuf + 2));
	return msgid;
}

int
handle_client(int clientfd, unsigned char *data, size_t datalen) {
	int ret = get_msgid(clientfd);
	if (ret < 0) {
		return ret;
	}
	uint16_t msgid = ret;
	size_t totalsent = 0;
	size_t queued = 0;
	struct iovec iov[1 * 3]; // must be multiple of 3
	const int totiovs = sizeof(iov) / sizeof(struct iovec);
	while (totalsent < datalen) {
		int skipiovs = 0;
		// fill vectors from scratch
		int fillediovs = 0;
		while (fillediovs < totiovs - 2) {
			// TCP preambule - msg length
			uint16_t msglen = ntohs(*((uint16_t *)(data + queued)));
			iov[fillediovs].iov_base = data + queued;
			iov[fillediovs].iov_len = sizeof(msglen);
			queued += iov[fillediovs].iov_len;
			fillediovs++;
			// replace the original msgid with new constant
			iov[fillediovs].iov_base = &msgid;
			iov[fillediovs].iov_len = sizeof(msgid);
			queued += iov[fillediovs].iov_len;
			fillediovs++;
			// append rest of the original message
			iov[fillediovs].iov_base = data + queued;
			// payload without msgid
			iov[fillediovs].iov_len = msglen - sizeof(msgid);
			queued += iov[fillediovs].iov_len;
			fillediovs++;
			assert(fillediovs <= totiovs);
			assert(queued <= datalen);
			if (queued == datalen) {
				break;
			}
		}
		assert(fillediovs > 0);
		assert(fillediovs <= totiovs);
		size_t batchsize = queued - totalsent;
		assert(batchsize <= datalen);
		// EINTR handling
		while (queued > totalsent) {
			assert(skipiovs >= 0);
			assert(skipiovs < fillediovs);
			ssize_t sentnow = writev(clientfd, iov + skipiovs,
						 fillediovs - skipiovs);
			if (sentnow < 0) {
				perror("writev");
				return totalsent;
			}
			assert((size_t)sentnow <= batchsize);
			totalsent += sentnow;
			assert(totalsent <= datalen);
			if (queued == totalsent) {
				break;
			}
			fprintf(stderr,
				"EINTR? sent in this cycle %ld but planned to "
				"send %ld\n",
				sentnow, batchsize);
			size_t skippedbytes = 0;
			while (skippedbytes < (size_t)sentnow) {
				assert(skipiovs >= 0);
				assert(skipiovs < fillediovs);
				if (iov[skipiovs].iov_len <= sentnow - skippedbytes) {
					// whole block was sent already
					skippedbytes += iov[skipiovs].iov_len;
					assert(skippedbytes <= batchsize);
					skipiovs++;
					assert(skipiovs <= fillediovs);
					continue;
				} else {
					// only part of this block was sent
					iov[skipiovs].iov_len -=
						(sentnow - skippedbytes);
					iov[skipiovs].iov_base +=
						(sentnow - skippedbytes);
					break;
				}
			}
		};
	}
	return totalsent;
}

int
main(int argc, char **argv) {
	if (argc != 3) {
		fprintf(stderr, "usage: %s port filename\n", argv[0]);
		exit(1);
	};
	int port = atoi(argv[1]);
	char *infilename = argv[2];

	int infiledes = open(infilename, 0);
	if (infiledes == -1) {
		fprintf(stderr, "failed to open file: %s: %s\n", infilename,
			strerror(errno));
		exit(2);
	};

	struct stat infilestat;
	if (fstat(infiledes, &infilestat) == -1) {
		perror("fstat");
		exit(2);
	}

	size_t insize = infilestat.st_size;
	void *data = mmap(0, insize, PROT_READ, MAP_SHARED, infiledes, 0);
	if (data == MAP_FAILED) {
		perror("mmap");
		exit(3);
	};

	int listenfd = get_socket(port);
	if (listenfd == -1) {
		exit(4);
	}
	int clientfd = accept(listenfd, NULL, NULL);
	if (clientfd == -1) {
		perror("accept");
		exit(5);
	}
	handle_client(clientfd, data, insize);
	if (close(clientfd) == -1) {
		perror("close");
		exit(6);
	}

	exit(0);
}
